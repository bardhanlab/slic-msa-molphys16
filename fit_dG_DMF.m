% Function: f(x)=a*|x|^0.5
%Par=[a,b]

%------------
% Curve fitting for T=-60
%------------
clc
A=[1.263997];
S=@(Par)sum((Par(1)*abs(xdata(1,:)).^0.5-ydata(1,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(1,:),ydata(1,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.263997];
VAL1=Par(1)*abs(xdata(1,:)).^0.5;
VAL2=ydata(1,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=-30
%------------

A=[1.303632];
S=@(Par)sum((Par(1)*abs(xdata(2,:)).^0.5-ydata(2,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(2,:),ydata(2,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.303632];
VAL1=Par(1)*abs(xdata(2,:)).^0.5;
VAL2=ydata(2,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=0
%------------

A=[1.343450];
S=@(Par)sum((Par(1)*abs(xdata(3,:)).^0.5-ydata(3,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(3,:),ydata(3,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.343450];
VAL1=Par(1)*abs(xdata(3,:)).^0.5;
VAL2=ydata(3,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=30
%------------

A=[1.382394];
S=@(Par)sum((Par(1)*abs(xdata(4,:)).^0.5-ydata(4,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(4,:),ydata(4,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.382393];
VAL1=Par(1)*abs(xdata(4,:)).^0.5;
VAL2=ydata(4,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=60
%------------

A=[1.419249];
S=@(Par)sum((Par(1)*abs(xdata(5,:)).^0.5-ydata(5,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(5,:),ydata(5,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.419249];
VAL1=Par(1)*abs(xdata(5,:)).^0.5;
VAL2=ydata(5,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=90
%------------

A=[1.452618];
S=@(Par)sum((Par(1)*abs(xdata(6,:)).^0.5-ydata(6,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(6,:),ydata(6,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.452618];
VAL1=Par(1)*abs(xdata(6,:)).^0.5;
VAL2=ydata(6,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=120
%------------

A=[1.481435];
S=@(Par)sum((Par(1)*abs(xdata(7,:)).^0.5-ydata(7,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(7,:),ydata(7,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.481435];
VAL1=Par(1)*abs(xdata(7,:)).^0.5;
VAL2=ydata(7,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)

Alpha=[1.263997 1.303632 1.343450 1.382394 1.419249 1.452618 1.481435];
