% Function: f(x)=a*|x|^0.5
%Par=[a,b]

%------------
% Curve fitting for T=15
%------------
clc
A=[1.145017];
S=@(Par)sum((Par(1)*abs(xdata(1,:)).^0.5-ydata(1,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(1,:),ydata(1,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.145017];
VAL1=Par(1)*abs(xdata(1,:)).^0.5;
VAL2=ydata(1,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=17
%------------
A=[1.146810];
S=@(Par)sum((Par(1)*abs(xdata(2,:)).^0.5-ydata(2,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(2,:),ydata(2,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.146810];
VAL1=Par(1)*abs(xdata(2,:)).^0.5;
VAL2=ydata(2,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=19
%------------
A=[1.148633];
S=@(Par)sum((Par(1)*abs(xdata(3,:)).^0.5-ydata(3,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(3,:),ydata(3,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.148633];
VAL1=Par(1)*abs(xdata(3,:)).^0.5;
VAL2=ydata(3,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=21
%------------
A=[1.150466];
S=@(Par)sum((Par(1)*abs(xdata(4,:)).^0.5-ydata(4,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(4,:),ydata(4,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.150466];
VAL1=Par(1)*abs(xdata(4,:)).^0.5;
VAL2=ydata(4,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=23
%------------
A=[1.152324];
S=@(Par)sum((Par(1)*abs(xdata(5,:)).^0.5-ydata(5,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(5,:),ydata(5,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.152324];
VAL1=Par(1)*abs(xdata(5,:)).^0.5;
VAL2=ydata(5,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=25
%------------
A=[1.154218];
S=@(Par)sum((Par(1)*abs(xdata(6,:)).^0.5-ydata(6,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(6,:),ydata(6,:),'o');
hold on
plot(xx,yy)
Par
Par=[1.154218];
VAL1=Par(1)*abs(xdata(6,:)).^0.5;
VAL2=ydata(6,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Alpha=[1.145017 1.146810 1.148633 1.150466 1.152324 1.154218];
