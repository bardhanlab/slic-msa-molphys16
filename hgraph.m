clc
clear all
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
TV=[0,25,50,75,100];    % Temperature vector for water
for i=1:length(TV)
    T = TV(i); % in C
    eps_in = 1.0;
    P = [-1.410*1e-6 9.398*1e-4 -0.40008 +87.740]; 
    eps_out = polyval(P,T);   % epsilon for water from Mamberg and Maryott 1956
    R_s=  1.42; % from Fawcett book p 108
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*eps_out]);  % Fawcett 2004,1992
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    eps_hat = (eps_out-eps_in)/eps_out;


    conv_factor = 332.112 * 4.184;  % from Angstroms/relative
                                    % permittivities/electron charges
                                    % into kJ/mol;

    %---------------------------------------------------------------------------
    %   Positive charge
    %---------------------------------------------------------------------------
    q = 1;
    R = linspace(0.7,10,1000);
    dGdNp = -q./R.^2;
    dG_bornp(i,:) = conv_factor * 0.5 * q * R .* (eps_hat*dGdNp);
    dG_MSAp(i,:)  = conv_factor * 0.5 * q * -eps_hat * q./(R+ delta_s);
    sigma_bornp(i,:) = dG_bornp(i,:)./(0.5*R*q*conv_factor);
    sigma_MSAp(i,:)  = dG_MSAp(i,:)./(0.5*R*q*conv_factor);
    sigma_bornp_over_MSAp(i,:) = sigma_bornp(i,:)./sigma_MSAp(i,:);
    hp(i,:) = sigma_bornp_over_MSAp(i,:) - 1;
    sigmaNLBCp(i,:)=sigma_bornp(i,:)./(1+hp(i,:));
    
    %---------------------------------------------------------------------------
    %   negetaive charge
    %---------------------------------------------------------------------------
    q = -1;
    dGdNn = -q./R.^2;
    dG_bornn(i,:) = conv_factor * 0.5 * q * R .* (eps_hat*dGdNn);
    dG_MSAn(i,:)  = conv_factor * 0.5 * q * -eps_hat * q./(R+ delta_s);
    sigma_bornn(i,:) = dG_bornn(i,:)./(0.5*R*q*conv_factor);
    sigma_MSAn(i,:)  = dG_MSAn(i,:)./(0.5*R*q*conv_factor);
    sigma_bornn_over_MSAn(i,:) = sigma_bornn(i,:)./sigma_MSAn(i,:);
    hn(i,:) = sigma_bornn_over_MSAn(i,:) - 1;
    sigmaNLBCn(i,:)=sigma_bornn(i,:)./(1+hn(i,:));
end
for i=1:length(TV)    
    xdatan(i,:)=-dGdNn-(-0.5)*sigmaNLBCn(i,:);
    xdatap(i,:)=-dGdNp-(-0.5)*sigmaNLBCp(i,:);
end
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------   
Alpha=[0.670785 0.685195 0.699823 0.714839 0.730188];  %Values of \alpha at temperatures in TV    
xx=-1:0.05:1;
for i=1:length(Alpha)   % values of h based on the suggested eq. h(E_n)=\alpha sqrt(E_n)
    hfunc(:,i)=Alpha(i)*sqrt(abs(xx));
end;
%---------------------------------------------------------------------------
figure(1);
plot(xdatap(2,:),hp(2,:),'--','Color',[255/255,127/255,50/255],'linewidth',2);    %function h (Born/MSA -1 ) @ T=25   (Positive Part)
hold on
plot(xdatap(4,:),hp(4,:),'Color',[0/255,204/255,153/255],'linewidth',2);     %function h (Born/MSA -1 ) @ T=75   (Positive Part)
set(gca,'fontsize',16);xlabel('E_n');% (10^{-10} N.C^{-1})'
ylabel('h(E_n)')
hold on
plot(xx,hfunc(:,2),'o','Color',[255/255,127/255,50/255],'MarkerFaceColor',[255/255,127/255,50/255]);    %function h (h(E_n)=\alpha sqrt(E_n) ) @ T=25
hold on
plot(xx,hfunc(:,4),'^','Color',[0/255,204/255,153/255],'MarkerFaceColor',[0/255,204/255,153/255]);      %function h (h(E_n)=\alpha sqrt(E_n) ) @ T=75

legend('T=25^\circC, Eq. 17','T=75^\circC, Eq. 17','T=25^\circC, Eq. 18','T=75^\circC, Eq. 18','location','southeast');

plot(xdatan(2,:),hn(2,:),'--','Color',[255/255,127/255,50/255],'linewidth',2);    %function h (Born/MSA -1 ) @ T=25   (Negative Part)
hold on
plot(xdatan(4,:),hn(4,:),'Color',[0/255,204/255,153/255],'linewidth',2);     %function h (Born/MSA -1 ) @ T=75   (Negative Part)
hold on
xlim([-1,1])