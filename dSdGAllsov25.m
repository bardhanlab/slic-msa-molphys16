clear all
clc
%---------------------------------------------------------------------------
RV=[0.88,1.16,1.52,1.63,1.84,1.19,1.67,1.82,2.06];% Radius Shannon and Prewitt in Angstroms Fawcett04
CI=[1,1,1,1,1,-1,-1,-1,-1];% charge sign index (Li+,Na+,K+,Rb+,Cs+,F-,Cl-,Br-,I-)

% Solvent: Water, Methanol, Formamide, Dimethylformamide, Acetonitrile
R_sV=[1.42 3.71/2 3.45/2 5.17/2 4.27/2]; % Solvent dipole radius For water from Fawcett book p 108
                                         % For others from Fawcett 92
                                        
TSV=[0,5,18,-60,15]; TEV=[100,55,25,120,25]; % the time interval for different solvents
                                             % TSV: Staring temperature, TEV: Ending Temperature    
T=25;   % Temperature of interest
eps_in = 1.0;   % Ion dielectric constant
conv_factor = 332.112 * 4.184;      % from Angstroms/relative
                                    % permittivities/electron charges
                                    % into kJ/mol;

%---------------------------Foe Paper------------------------------------
% Creating tables for the Molphys paper
TABLE_W=zeros(9,6);TABLE_MeOH=zeros(9,6);TABLE_F=zeros(9,6);
TABLE_DMF=zeros(9,6);TABLE_AN=zeros(9,6);
%---------------------------------------------------------------------------
% Temperature Range for solvents                                    
T_Water=[0,25,50,75,100];
T_Methanol=[5 15 25 35 45 55];
T_Formamide=[18 19 20 21 22 23 24 25];
T_Nithromethane=[12 32 52 72 92];
T_Acetonitrile=[15 17 19 21 23 25];
T_DMF=[-60 -30 0 30 60 90 120];
%---------------------------------------------------------------------------
% Alpha for solvents at temperatures presented above
Alpha_Water=[0.670785 0.685195 0.699823 0.714839 0.730188];
Alpha_Methanol=[1.012168 1.023760 1.035502 1.047330 1.059305 1.071471];
Alpha_Formamide=[0.770913 0.771804 0.772783 0.773701 0.774657 0.775666 0.776613 0.777623];
%Alpha_Acetone=[1.087680 1.109566 1.131996 1.154926 1.178421 1.202554];%WRONG
Alpha_Acetonitrile=[1.145017 1.146810 1.148633 1.150466 1.152324 1.154218];
Alpha_DMF=[1.263997 1.303632 1.343450 1.382394 1.419249 1.452618 1.481435];
%---------------------------------------------------------------------------
Tvec={T_Water T_Methanol T_Formamide T_DMF T_Acetonitrile};
Alphavec={Alpha_Water Alpha_Methanol Alpha_Formamide Alpha_DMF Alpha_Acetonitrile};
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------                                   
for i=1:length(RV)
    if CI(i)==1
        q=1;
    elseif CI(i)==-1
        q=-1;
    end
    R=RV(i);
    dGdN = -q/R^2;  
    for j=1:length(R_sV)
        R_s=R_sV(j);
        Ts=TSV(j);
        Te=TEV(j);
        Tval=cell2mat(Tvec(j));
        Alphaval=cell2mat(Alphavec(j));        
        if j==1
            P = [-1.410*1e-6 9.398*1e-4 -0.40008 +87.740];
            eps_out_f = @(X) polyval(P,X);   % epsilon for water from Malmberg and Maryott 1956
            eps_out=eps_out_f(T);
            P_p=[-1.410*1e-6*3 9.398*1e-4*2 -0.40008];  
            eps_out_p=polyval(P_p,T);    % Derivative of epsilon_water
            alpha_f = @(X) 0.000594*X+0.670476;
            alpha=alpha_f(T);
            alpha_p=0.000594;  % Derivative of Alpha  
        elseif j==2
            eps_out_f= @(X) 10^(log10(32.63)-0.264e-2*(X-25)); % epsilon for methanol from Maryott and smith 1951
            eps_out=eps_out_f(T);
            eps_out_p=0.264e-2*log(10)*(-10^(log10(32.63)-0.264e-2*(T-25)));
            alpha_f = @(X) 0.001186*X+1.006020;
            alpha=alpha_f(T);
            alpha_p=0.001186;  % Derivative of Alpha  
        elseif j==3
            eps_out_f= @(X) 109-72e-2*(X-20); % epsilon for formamide from Maryott and smith 1951
            eps_out=eps_out_f(T);
            eps_out_p=-72e-2;
            alpha_f = @(X) 0.000960*X+0.753585;
            alpha=alpha_f(T);
            alpha_p=0.000960;  % Derivative of Alpha  
            
        elseif j==4
            P = [-1.000389*1e-6 7.718531*1e-4 -2.204448e-1 +4.204569e1];   % epsilon for Dimethylformamide from Bass
            eps_out_f = @(X) polyval(P,X);         
            eps_out=eps_out_f(T);
            P_p=[-1.000389*1e-6*3 7.718531*1e-4*2 -2.204448e-1];  
            eps_out_p=polyval(P_p,T);    % Derivative of epsilon_Dimethylformamide
            alpha_f = @(X) 0.001222*X+1.341465;
            alpha=alpha_f(T);
            alpha_p=0.001222;  % Derivative of Alpha  
        elseif j==5
            eps_out_f= @(X) 37.5-16e-2*(X-20); % epsilon for acetonitrile from Maryott and smith 1951
            eps_out=eps_out_f(T);
            eps_out_p=-16e-2;
            alpha_f = @(X) 0.000920*X+1.131184;
            alpha=alpha_f(T);
            alpha_p=0.000920;  % Derivative of Alpha  
        end
        ERROR=1e10;
        eps_hat = (eps_out-eps_in)/eps_out;
        s_NLBC0=eps_hat*dGdN;  % Induced surface charge based on HSBC/MSA model
        
        while ERROR>1e-12
            s_NLBC=s_NLBC0;
            E_n=-dGdN-(-0.5)*s_NLBC;
            hf=alpha*sqrt(abs(E_n));
            s_NLBC0=eps_hat*dGdN/(1+hf);
            ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
            ERROR2=max(abs(s_NLBC0-s_NLBC));
            ERROR=max(ERROR1,ERROR2);
        end
        s_NLBC=s_NLBC0;    
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        rootsOfWertheim = roots([1 4 6 4 1 0 -16*eps_out]);
        lambda_s = real(rootsOfWertheim(end));
        delta_s  = R_s / lambda_s;   
        eps_hat_p=(eps_out_p*eps_out-eps_out_p*(eps_out-eps_in))/(eps_out)^2;
        h_p=hder(Tval,T,eps_in,R,eps_out_f,q,alpha_f);
        %h_p=alpha_p*sqrt(abs(-dGdN));    % Defrivative of h(En)  
%---------------------------------------------------------------------------
%     Entropy Results
%---------------------------------------------------------------------------                        
        ds_NLBC(i,j)=1000*-0.5*conv_factor*R*dGdN/q*q^2*(eps_hat_p*(1+hf)-h_p*eps_hat)/(1+hf)^2;
        ds_Born(i,j)=1000*-0.5*conv_factor*R*dGdN*q*eps_hat_p;
        ds_MSA(i,j)=1000*0.5*conv_factor*q^2*MSAder(R_s,Tval,T,eps_in,R,eps_out_f);        
%---------------------------------------------------------------------------        
        
%---------------------------------------------------------------------------
%       Gibbs Solvation Free Energy
%---------------------------------------------------------------------------
        dG_Born(i,j) = conv_factor * 0.5 * q * R * (eps_hat*dGdN);
        dG_MSA(i,j)  = conv_factor * 0.5 * q * -eps_hat * q/(R+ delta_s);
        dG_NLBC(i,j)=0.5*conv_factor*R*s_NLBC*q;        
    end   
end
%---------------------------------------------------------------------------
%       Fill the tables
%---------------------------------------------------------------------------
TABLE_W(:,1)=dG_Born(:,1);TABLE_MeOH(:,1)=dG_Born(:,2);
TABLE_F(:,1)=dG_Born(:,3);TABLE_DMF(:,1)=dG_Born(:,4);TABLE_AN(:,1)=dG_Born(:,5);

TABLE_W(:,2)=dG_MSA(:,1);TABLE_MeOH(:,2)=dG_MSA(:,2);
TABLE_F(:,2)=dG_MSA(:,3);TABLE_DMF(:,2)=dG_MSA(:,4);TABLE_AN(:,2)=dG_MSA(:,5);

TABLE_W(:,3)=dG_NLBC(:,1);TABLE_MeOH(:,3)=dG_NLBC(:,2);
TABLE_F(:,3)=dG_NLBC(:,3);TABLE_DMF(:,3)=dG_NLBC(:,4);TABLE_AN(:,3)=dG_NLBC(:,5);

TABLE_W(:,4)=ds_Born(:,1);TABLE_MeOH(:,4)=ds_Born(:,2);
TABLE_F(:,4)=ds_Born(:,3);TABLE_DMF(:,4)=ds_Born(:,4);TABLE_AN(:,4)=ds_Born(:,5);

TABLE_W(:,5)=ds_MSA(:,1);TABLE_MeOH(:,5)=ds_MSA(:,2);
TABLE_F(:,5)=ds_MSA(:,3);TABLE_DMF(:,5)=ds_MSA(:,4);TABLE_AN(:,5)=ds_MSA(:,5);

TABLE_W(:,6)=ds_NLBC(:,1);TABLE_MeOH(:,6)=ds_NLBC(:,2);
TABLE_F(:,6)=ds_NLBC(:,3);TABLE_DMF(:,6)=ds_NLBC(:,4);TABLE_AN(:,6)=ds_NLBC(:,5);

TABLE_W=int16(TABLE_W);
TABLE_MeOH=int16(TABLE_MeOH);
TABLE_F=int16(TABLE_F);
TABLE_DMF=int16(TABLE_DMF);
TABLE_AN=int16(TABLE_AN);
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------


