Here are the matlab codes that have been developed for the results of this paper.

hgraph.m: Produce the Fig. 1 of the paper.

Alphagraph.m: Produce the Fig. 2 of the paper.

plotGAAllSon: Produce the graphical abstract figures, not only for water, but also for all the other solvents.

dSdGAllsov25: Claculates the free solvation energy and entropies of different ions in different solvents at T=25 C for the Tables 2, 4, 5, 6, and 7.

The following codes are used for finding the parameters:

dG_W, fit_dG_W, dG_MeOH, fit_dG_MeOH, dG_F, fit_dG_F, dG_AN, fit_dG_AN, dG_DMF, fit_dG_DMF: are used to find the optimum parameter alpha at different temperatures.