function h_p=hder(Tval,T,epsin,R,eps_out_f,q,alpha_f)
h=1e-4;

if T==Tval(1)
    T1=T;      
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q/R^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*sqrt(abs(E_n));

    T2=T+h;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*sqrt(abs(E_n));
    h_p=(hf2-hf1)/h;
elseif T==Tval(end)
    T1=T-h;   
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q/R^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*sqrt(abs(E_n));

    T2=T;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*sqrt(abs(E_n));
    h_p=(hf2-hf1)/h;
else
    T1=T-h;   
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q/R^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*sqrt(abs(E_n));

    T2=T+h;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*sqrt(abs(E_n));
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*sqrt(abs(E_n));
    h_p=(hf2-hf1)/2/h;
end
    
    