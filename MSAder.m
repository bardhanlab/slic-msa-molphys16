function a_p=MSAder(R_s,Tval,T,epsin,R,eps_out_f)

h=1e-4;

if T==Tval(1)
    T1=T;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var1=epshat/(R+delta_s);
    T2=T+h;     
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var2=epshat/(R+delta_s);
    a_p=(Var2-Var1)/h;
elseif T==Tval(end)
    T1=T-h;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var1=epshat/(R+delta_s);
    T2=T;      
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var2=epshat/(R+delta_s);
    a_p=(Var2-Var1)/h;
else
    T1=T-h;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var1=epshat/(R+delta_s);
    T2=T+h;      
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*epsout]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    Var2=epshat/(R+delta_s);
    a_p=(Var2-Var1)/(2*h);
end
    
    
    
    
