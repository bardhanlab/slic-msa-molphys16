% Function: f(x)=a*|x|^0.5
%Par=[a,b]

%------------
% Curve fitting for T=18
%------------
clc
A=[0.770913];
S=@(Par)sum((Par(1)*abs(xdata(1,:)).^0.5-ydata(1,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(1,:),ydata(1,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.770913];
VAL1=Par(1)*abs(xdata(1,:)).^0.5;
VAL2=ydata(1,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=19
%------------
A=[0.771804];
S=@(Par)sum((Par(1)*abs(xdata(2,:)).^0.5-ydata(2,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(2,:),ydata(2,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.771804];
VAL1=Par(1)*abs(xdata(2,:)).^0.5;
VAL2=ydata(2,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=20
%------------
A=[0.772783];
S=@(Par)sum((Par(1)*abs(xdata(3,:)).^0.5-ydata(3,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(3,:),ydata(3,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.772783];
VAL1=Par(1)*abs(xdata(3,:)).^0.5;
VAL2=ydata(3,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=21
%------------
A=[0.773701];
S=@(Par)sum((Par(1)*abs(xdata(4,:)).^0.5-ydata(4,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(4,:),ydata(4,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.773701];
VAL1=Par(1)*abs(xdata(4,:)).^0.5;
VAL2=ydata(4,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=22
%------------
A=[0.774657];
S=@(Par)sum((Par(1)*abs(xdata(5,:)).^0.5-ydata(5,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(5,:),ydata(5,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.774657];
VAL1=Par(1)*abs(xdata(5,:)).^0.5;
VAL2=ydata(5,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=23
%------------
A=[0.775666];
S=@(Par)sum((Par(1)*abs(xdata(6,:)).^0.5-ydata(6,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(6,:),ydata(6,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.775666];
VAL1=Par(1)*abs(xdata(6,:)).^0.5;
VAL2=ydata(6,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=24
%------------
A=[0.776613];
S=@(Par)sum((Par(1)*abs(xdata(7,:)).^0.5-ydata(7,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(7,:),ydata(7,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.776613];
VAL1=Par(1)*abs(xdata(7,:)).^0.5;
VAL2=ydata(7,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=25
%------------
A=[0.777623];
S=@(Par)sum((Par(1)*abs(xdata(8,:)).^0.5-ydata(8,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(8,:),ydata(8,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.777623];
VAL1=Par(1)*abs(xdata(8,:)).^0.5;
VAL2=ydata(8,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)


Alpha=[0.770913 0.771804 0.772783 0.773701 0.774657 0.775666 0.776613 0.777623];
