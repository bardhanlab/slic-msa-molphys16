clc
clear all
TV=[0,25,50,75,100];
for i=1:length(TV)
    T = TV(i); % in C
    eps_in = 1.0;
    P = [-1.410*1e-6 9.398*1e-4 -0.40008 +87.740];
    eps_out = polyval(P,T);
    R_s=  1.42; % from Fawcett book p 108
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*eps_out]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    eps_hat = (eps_out-eps_in)/eps_out;


    conv_factor = 332.112 * 4.184;  % from Angstroms/relative
                                    % permittivities/electron charges
                                    % into kJ/mol;

    q = 1;
    R = linspace(1,100,1000);
    dGdN = -q./R.^2;
    dG_born(i,:) = conv_factor * 0.5 * q * R .* (eps_hat*dGdN);
    dG_MSA(i,:)  = conv_factor * 0.5 * q * -eps_hat * q./(R+ delta_s);

    


    sigma_born(i,:) = dG_born(i,:)./(0.5*R*q*conv_factor);
    sigma_MSA(i,:)  = dG_MSA(i,:)./(0.5*R*q*conv_factor);

    sigma_born_over_MSA(i,:) = sigma_born(i,:)./sigma_MSA(i,:);

    h(i,:) = sigma_born_over_MSA(i,:) - 1;
    
    sigma_HSBC(i,:)=sigma_born(i,:)./(1+h(i,:));
    
end


figure;
    plot(R,dG_born,'b','linewidth',2);
    hold; set(gca,'fontsize',16);xlabel('Radius R (Angstrom)');
    ylabel('\Delta G (kJ/mol)');
    plot(R,dG_MSA,'r','linewidth',2);
    legend('Born','MSA');
    
    
    
figure;
    plot(R,h);
    set(gca,'fontsize',16);xlabel('Radius R (Angstrom)');
    ylabel('h(R)')


    figure;
    plot(-dGdN,h);
    set(gca,'fontsize',16);xlabel('Electric field');
    ylabel('h(|En|)')

for i=1:length(TV)    
    xdata(i,:)=abs(-dGdN-(-0.5)*sigma_HSBC(i,:));
end
 ydata=h;

