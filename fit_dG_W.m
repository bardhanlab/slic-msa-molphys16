% Function: f(x)=a*|x|^0.5
%Par=[a,b]

%------------
% Curve fitting for T=0
%------------
clc
A=[0.670785];
S=@(Par)sum((Par(1)*abs(xdata(1,:)).^0.5-ydata(1,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(1,:),ydata(1,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.670785];
VAL1=Par(1)*abs(xdata(1,:)).^0.5;
VAL2=ydata(1,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
set(gca,'fontsize',16);xlabel('Electric field');
    ylabel('h(|En|)')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------
% Curve fitting for T=25
%------------
%clc
A=[0.685195];
S=@(Par)sum((Par(1)*abs(xdata(2,:)).^0.5-ydata(2,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(2,:),ydata(2,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.685195];
VAL1=Par(1)*abs(xdata(2,:)).^0.5;
VAL2=ydata(2,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=50
%------------
%clc
A=[0.699823];
S=@(Par)sum((Par(1)*abs(xdata(3,:)).^0.5-ydata(3,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(3,:),ydata(3,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.699823];
VAL1=Par(1)*abs(xdata(3,:)).^0.5;
VAL2=ydata(3,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=75
%------------
%clc
A=[0.714839];
S=@(Par)sum((Par(1)*abs(xdata(4,:)).^0.5-ydata(4,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(4,:),ydata(4,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.714839];
VAL1=Par(1)*abs(xdata(4,:)).^0.5;
VAL2=ydata(4,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------
% Curve fitting for T=100
%------------
%clc
A=[0.730188];
S=@(Par)sum((Par(1)*abs(xdata(5,:)).^0.5-ydata(5,:)).^2);
Par=fminsearch(S,A);
xx=0.001:0.01:1;
yy = Par(1)*abs(xx).^0.5;
figure(1)
plot(xdata(5,:),ydata(5,:),'o');
hold on
plot(xx,yy)
Par
Par=[0.730188];
VAL1=Par(1)*abs(xdata(5,:)).^0.5;
VAL2=ydata(5,:);
ER=abs((VAL1-VAL2)./VAL2)*100;
MER=max(ER)

Alpha=[0.670785 0.685195 0.699823 0.714839 0.730188];
