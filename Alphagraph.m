clear all
clc
%---------------------------------------------------------------------------
% Temperature Range for different solvents.
%---------------------------------------------------------------------------
T_Water=[0,25,50,75,100];
T_Methanol=[5 15 25 35 45 55];
T_Formamide=[18 19 20 21 22 23 24 25];
%T_Acetone=[-60 -40 -20 0 20 40];
T_Acetonitrile=[15 17 19 21 23 25];
T_DMF=[-60 -30 0 30 60 90 120];
%---------------------------------------------------------------------------
% Values for \alpha at different temperatures presented in above lines.
%---------------------------------------------------------------------------
Alpha_Water=[0.670785 0.685195 0.699823 0.714839 0.730188];
Alpha_Methanol=[1.012168 1.023760 1.035502 1.047330 1.059305 1.071471];
Alpha_Formamide=[0.770913 0.771804 0.772783 0.773701 0.774657 0.775666 0.776613 0.777623];
%Alpha_Acetone=[1.087680 1.109566 1.131996 1.154926 1.178421 1.202554];%WRONG
Alpha_Acetonitrile=[1.145017 1.146810 1.148633 1.150466 1.152324 1.154218];
Alpha_DMF=[1.263997 1.303632 1.343450 1.382394 1.419249 1.452618 1.481435];
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
% create the main plot for all solvents
figure(1)
plot(T_Water,Alpha_Water,'Color',[255/255,127/255,50/255],'Marker','o','MarkerFaceColor',[255/255,127/255,50/255],'linewidth',2)
hold on; set(gca,'fontsize',16);xlabel('Temperature (^{\circ}C)');
    ylabel('\alpha (Angstroms)');
    txt_w1='\uparrow';
    txt_w2='R^2=0.9998';
    text(50,0.67,txt_w1,'FontSize',16);
    text(40,0.63,txt_w2,'FontSize',16);
plot(T_Methanol,Alpha_Methanol,'-b^','MarkerFaceColor','b','linewidth',2)
    txt_m1='\uparrow';
    txt_m2='R^2=0.9999';
    text(25,1.01,txt_m1,'FontSize',16);
    text(16,0.97,txt_m2,'FontSize',16);
hold on
plot(T_Formamide,Alpha_Formamide,'-mx','linewidth',2)
    txt_f1='\downarrow';
    txt_f2='R^2=0.9998';
    text(21,0.805,txt_f1,'FontSize',16);
    text(7,0.85,txt_f2,'FontSize',16);
hold on
plot(T_Acetonitrile,Alpha_Acetonitrile,'Color',[0/255,204/255,153/255],'MarkerFaceColor',[0/255,204/255,153/255],'Marker','v','linewidth',2)
txt_a1='\downarrow';
txt_a2='R^2=0.9999';
text(20,1.185,txt_a1,'FontSize',16);
text(10,1.22,txt_a2,'FontSize',16);
hold on
plot(T_DMF,Alpha_DMF,'-k+','linewidth',2)
txt_d1='\downarrow';
txt_d2='R^2=0.9972';
text(30,1.41,txt_d1,'FontSize',16);
text(22,1.445,txt_d2,'FontSize',16);
legend('W','MeOH','F','AN','DMF','location','southwest');
xlim([-60,120])
grid on
%---------------------------------------------------------------------------
% create the inset plot for AN
%---------------------------------------------------------------------------
axes('Position',[.69 .59 .2 .2])
box on
plot(T_Acetonitrile,Alpha_Acetonitrile,'Color',[0/255,204/255,153/255],'Marker','v','MarkerFaceColor',[0/255,204/255,153/255],'linewidth',2)
set(gca,'fontsize',16);
handle=title('AN');
set(handle,'Position',[20,1.152]);

%---------------------------------------------------------------------------
% create the inset plot for F
%---------------------------------------------------------------------------
axes('Position',[.69 .27 .2 .2])
box on
plot(T_Formamide,Alpha_Formamide,'-mx','linewidth',2)
set(gca,'fontsize',16);
handle=title('F');
set(handle,'Position',[20,0.777]);