% Formamide
% Reference: Table of dielectric constants of pure liquids
% epsilon_tp=epsilon_t-a(tp-t)
% log10(epsilon_tp)=log10(epsilon_t)-alpha(tp-t)
% Maryott & Smith 1951
%   epsilon       temp(C)         a or alpha                range
% -----------    ----------  -------------------         ----------
%   109             20            a=72e-2                    18,25

% R_s=3.45/2

%----------------------------------------------------------------------
%----------------------------------------------------------------------


clc
clear all
TV=[18 19 20 21 22 23 24 25];
for i=1:length(TV)
    T = TV(i); % in C
    eps_in = 1.0;
    eps_out=109-72e-2*(T-20);
    R_s=  3.45/2; % from Fawcett paper 92
    rootsOfWertheim = roots([1 4 6 4 1 0 -16*eps_out]);
    lambda_s = real(rootsOfWertheim(end));
    delta_s  = R_s / lambda_s;
    eps_hat = (eps_out-eps_in)/eps_out;


    conv_factor = 332.112 * 4.184;  % from Angstroms/relative
                                    % permittivities/electron charges
                                    % into kJ/mol;

    q = 1;
    R = linspace(1,100,1000);
    dGdN = -q./R.^2;
    dG_born(i,:) = conv_factor * 0.5 * q * R .* (eps_hat*dGdN);
    dG_MSA(i,:)  = conv_factor * 0.5 * q * -eps_hat * q./(R+ delta_s);

    


    sigma_born(i,:) = dG_born(i,:)./(0.5*R*q*conv_factor);
    sigma_MSA(i,:)  = dG_MSA(i,:)./(0.5*R*q*conv_factor);

    sigma_born_over_MSA(i,:) = sigma_born(i,:)./sigma_MSA(i,:);

    h(i,:) = sigma_born_over_MSA(i,:) - 1;
    sigma_HSBC(i,:)=sigma_born(i,:)./(1+h(i,:));   
end


figure;
    plot(R,dG_born,'b','linewidth',2);
    hold; set(gca,'fontsize',16);xlabel('Radius R (Angstrom)');
    ylabel('\Delta G (kJ/mol)');
    plot(R,dG_MSA,'r','linewidth',2);
    legend('Born','MSA');
    
    
    
figure;
    plot(R,h);
    set(gca,'fontsize',16);xlabel('Radius R (Angstrom)');
    ylabel('h(R)')


    figure;
    plot(-dGdN,h);
    set(gca,'fontsize',16);xlabel('Electric field');
    ylabel('h(|En|)')

for i=1:length(TV)    
    xdata(i,:)=abs(-dGdN-(-0.5)*sigma_HSBC(i,:));
end
ydata=h;

